FROM maven:3.5-jdk-8
MAINTAINER linzhw
RUN mkdir -p /data/files
ADD . /data

VOLUME ["/data/files"]
WORKDIR /data
EXPOSE 8080
CMD ["mvn", "-s", "settings.xml", "-T4.0C", "clean", "spring-boot:run", "-Dspring-boot.run.profiles=docker", "-Dspring.profiles.active=docker"]
#, "--spring.profiles.active=docker"
#RUN mvn -s settings.xml clean spring-boot:run -Dspring-boot.run.profiles=docker